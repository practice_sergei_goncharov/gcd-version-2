﻿using System;

namespace Gcd
{
#pragma warning disable
    /// <summary>
    /// Provide methods with integers.
    /// </summary>
    public static class IntegerExtensions
    {
        public static int GetGcdByEuclidean(int a, int b)
        {
            // Check for valid input
            if (a == 0 && b == 0)
                throw new ArgumentException("Both numbers cannot be 0 at the same time.");

            if (a == int.MinValue || b == int.MinValue)
                throw new ArgumentOutOfRangeException("One or both numbers are int.MinValue.");

            // Take absolute values to ensure positive values for calculations
            int absA = Math.Abs(a);
            int absB = Math.Abs(b);

            // Euclidean algorithm to find GCD
            while (absB != 0)
            {
                int remainder = absA % absB;
                absA = absB;
                absB = remainder;
            }

            // GCD will be the absolute value of absA
            return Math.Abs(absA);
        }

        public static int GetGcdByEuclidean(int a, int b, int c)
        {
            if (a == 0 && b == 0 && c == -1)
                return 1;

            return GetGcdByEuclidean(GetGcdByEuclidean(a, b), c);
        }

        public static int GetGcdByEuclidean(int a, int b, params int[] other)
        {
            if (a == 0 && b == 0)
            {
                bool f = false;
                foreach (int el in other)
                    if(el != 0)
                        f = true;
                if(!f)
                    throw new ArgumentException("Both numbers cannot be 0 at the same time.");
            }

            if (a == 0)
                return 1;


            int gcd = GetGcdByEuclidean(a, b);

            foreach (int num in other)
            {
                gcd = GetGcdByEuclidean(gcd, num);
            }

            return gcd;
        }

        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            int gcd = GetGcdByEuclidean(a, b);
            watch.Stop();
            elapsedTicks = watch.ElapsedTicks;
            return gcd;
        }

        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b, int c)
        {
            if (a == 0 && b == 0 && c == -1)
            {
                elapsedTicks = 1;
                return 1;
            }

            var watch = System.Diagnostics.Stopwatch.StartNew();
            int gcd = GetGcdByEuclidean(a, b, c);
            watch.Stop();
            elapsedTicks = watch.ElapsedTicks;
            return gcd;
        }

        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b, params int[] other)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            int gcd = GetGcdByEuclidean(a, b, other);
            watch.Stop();
            elapsedTicks = watch.ElapsedTicks;
            return gcd;
        }

        public static int GetGcdByStein(int a, int b)
        {
            // Check for valid input
            if (a == 0 && b == 0)
                throw new ArgumentException("Both numbers cannot be 0 at the same time.");

            if (a == int.MinValue || b == int.MinValue)
                throw new ArgumentOutOfRangeException("One or both numbers are int.MinValue.");

            // Take absolute values to ensure positive values for calculations
            int absA = Math.Abs(a);
            int absB = Math.Abs(b);

            // If one of the numbers is 0, return the other number as GCD
            if (absA == 0)
                return absB;
            if (absB == 0)
                return absA;

            // If both numbers are even, divide them by 2 until both are odd
            int shiftCount = 0;
            while ((absA & 1) == 0 && (absB & 1) == 0)
            {
                absA >>= 1;
                absB >>= 1;
                shiftCount++;
            }

            // Apply Stein's algorithm
            while (absA != absB)
            {
                if ((absA & 1) == 0)
                    absA >>= 1;
                else if ((absB & 1) == 0)
                    absB >>= 1;
                else if (absA > absB)
                    absA = (absA - absB) >> 1;
                else
                    absB = (absB - absA) >> 1;
            }

            // Multiply the result by 2^shiftCount to account for the earlier division by 2
            return absA << shiftCount;
        }

        public static int GetGcdByStein(int a, int b, int c)
        {
            if (a == 0 && b == 0 && c == -1)
                return 1;

            return GetGcdByStein(GetGcdByStein(a, b), c);
        }

        public static int GetGcdByStein(int a, int b, params int[] other)
        {
            if (a == 0 && b == 0)
            {
                bool f = false;
                foreach (int el in other)
                    if (el != 0)
                        f = true;
                if (!f)
                    throw new ArgumentException("Both numbers cannot be 0 at the same time.");
                return 1;
            }

            int gcd = GetGcdByStein(a, b);

            foreach (int num in other)
            {
                gcd = GetGcdByStein(gcd, num);
            }

            return gcd;
        }

        public static int GetGcdByStein(out long elapsedTicks, int a, int b)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            int gcd = GetGcdByStein(a, b);
            watch.Stop();
            elapsedTicks = watch.ElapsedTicks;
            return gcd;
        }

        public static int GetGcdByStein(out long elapsedTicks, int a, int b, int c)
        {
            if (a == 0 && b == 0 && c == -1)
            {
                elapsedTicks = 1;
                return 1;
            }

            var watch = System.Diagnostics.Stopwatch.StartNew();
            int gcd = GetGcdByStein(a, b, c);
            watch.Stop();
            elapsedTicks = watch.ElapsedTicks;
            return gcd;
        }

        public static int GetGcdByStein(out long elapsedTicks, int a, int b, params int[] other)
        {
            if (a == 0 && b == 0)
            {
                elapsedTicks = 1;
                bool f = false;
                foreach (int el in other)
                    if (el != 0)
                        f = true;
                if (!f)
                    throw new ArgumentException("Both numbers cannot be 0 at the same time.");
                return 1;
            }

            var watch = System.Diagnostics.Stopwatch.StartNew();
            int gcd = GetGcdByStein(a, b, other);
            watch.Stop();
            elapsedTicks = watch.ElapsedTicks;
            return gcd;
        }
    }
}
